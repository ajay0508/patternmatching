"use strict";

// RegExp or pattern matching

// Syntax string.match(regexp)

/*
	Example explained:

	/ain/g  is a regular expression.
	ain is a pattern (to be used in a search).
	g is a modifier perform a global match (find all matches rather than stopping after the first match)

	Modifiers:-

	g :- Perform a global match (find all matches rather than stopping after the first match)
	i :- Perform case-insensitive matching
	m :- Perform multiline matching
*/
let str ='';

/*function f1(str) {
	let res = str.match(/ain/g);
	console.log("Output of f1 ==> ",res);
}
let str = `The rain in SPAIN stays mainly in the plain`;
f1(str);

function f2(str) {
	let res = str.match(/ain/i);
	console.log("Output of f2 ==> ",res);
	console.log("Output of f2 ==> ",res[0])
}
str = `The rain in SPAIN stays mainly in the plain`;
f2(str);

function f3(str) {
	let res = str.match(/^ain/m);
	console.log("Output of f3 ==> ",res);
}
str = `The rain in SPAIN stays mainly in the plain`;
f3(str);

function f4(str) {
	let res = str.match(/ain/igm);
	console.log("Output of f4 ==> ",res);
}
str = `The rain in SPAIN stays mainly in the plain`;
f4(str);

function f5(str) {
	// Find any character between the brackets (any digit)
	let res = str.match(/[1-4]/g);
	console.log("Output of f5 ==> ",res);
}
str = `4123567894`;
f5(str);

function f6(str) {
	// Find any character NOT between the brackets (any non-digit)
	let res = str.match(/[^1-4]/g);
	console.log("Output of f6 ==> ",res);
}
str = `123456789A`;
f6(str);*/

/*function f7() {
	// Find any character between the brackets
  	let res = str.match(/[a-i|1-4]/g);
	console.log("Output of f7 ==> ",res);  
}
str = `Is this all there is? 1,2,3,7,8,9`;
f7(str);*/

/*function f8() {
	// Find any of the alternatives specified
  	let res = str.match(/(red|green)/g);
	console.log("Output of f8 ==> ",res);   
}
str = `re, green, red, green, gren, gr, blue, yellow`;
f8(str);*/

/*function f9() {	
  	let res = str.match(/(\d\d\d\d\d\d)/g);
	console.log("Output of f9 ==> ",res);   
}
str = `re, green, 123 red, 23, green, 666, gren, gr, 77, blue, 2, yellow`;
f9(str);*/

// str = `re, green, 123 red, 23, green, 666, gren, gr, 77, blue, 2, yellow`;
str = `123 34 56  @   @\ ajay . kumar shukla .  \  2  `;

// let text = str.replace(/^\s+/g, '');
// let text = str.replace(/\s+/g, '');
// let text = str.replace(/\d\d\d/g, 'D');
// let text = str.replace(/\d{2,3}/g, 'D');

console.log("Output of replace ==> ",str, "=");
let text = str.replace(/\./g, '9');


console.log("Output of replace ==> ",text, "=");

